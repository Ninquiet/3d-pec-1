using CarRecord;

namespace Settings
{
    public static class GameSettings
    {
        private static float GamePerformance = 1.0f;
        
        public static void SetGamePerformance(float performance)
        {
            GamePerformance = performance;
            OnPerformanceChanged();
        }

        private static void OnPerformanceChanged()
        {
            RecordLogger.ChangeTimeBetweenSamples(GamePerformance);
        }
    }
}