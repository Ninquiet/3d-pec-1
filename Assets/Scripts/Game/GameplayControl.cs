using System;
using CarRecord;
using Cinemachine;
using UnityEngine;

namespace Game
{
    public class GameplayControl : MonoBehaviour
    {
        public Action OnGameReset;
        public Action OnGameStarted;
        public Action OnGameFinished;
        
        [Header("UI")]
        [SerializeField] private GameUIController _gameUIController;
        [Header("Player")]
        [SerializeField] private CarRecordManager _carReferenceRecordManager;
        [SerializeField] private CarRecordManager _gosthReferenceRecordManager;
        [SerializeField] private Transform _playerStartReference;
        [Header("Record Data")]
        [SerializeField] private CarRecordData _carRecordDataLap1;
        [SerializeField] private CarRecordData _carRecordDataLap2;
        [SerializeField] private CarRecordData _carRecordDataLap3;
        [Header("Camera")]
        [SerializeField] private CinemachineVirtualCamera _virtualCamera;
        
        private bool _firstTriggerTouched;
        private bool _secondTriggerTouched;
        private bool _gameStarted;
        private bool _gameFinished;
        private int _currentLap = 0;
        private GameObject _playerCar;
        private GameObject _gosthCar;
        private CarRecordManager _carRecordManager;
        private CarRecordManager _gosthRecordManager;
        
        public GameObject GetCurrentCar => _playerCar;

        public void ResetGame()
        {
            if (_playerCar != null)
                Destroy(_playerCar);
            _playerCar = Instantiate(_carReferenceRecordManager.gameObject, _playerStartReference.position, _playerStartReference.rotation);
            _carRecordManager = _playerCar.GetComponent<CarRecordManager>();
            _firstTriggerTouched = false;
            _secondTriggerTouched = false;
            _gameStarted = false;
            _gameFinished = false;
            _currentLap = 0;
            _carReferenceRecordManager.StopRecording();
            Destroy(_gosthCar);
            OnGameReset?.Invoke();
        }

        public void PlayerTouchedFirstTrigger()
        {
            _firstTriggerTouched = true;
        }
        
        public void PlayerTouchedSecondTrigger()
        {
            _secondTriggerTouched = true;
            _firstTriggerTouched = false;
        }
        
        private void Start()
        {
            ResetGame();
        }
        
        private void StartGameplay()
        {
            _gameStarted = true;
            _currentLap = 1;
            _carRecordManager.SetRecordData(_carRecordDataLap1);
            _carRecordManager.StartRecording();
            GenerateGhostCar();
            OnGameStarted?.Invoke();
        }

        private void Update()
        {
            if (_gameStarted)
                SetLapTime();
            
            if (!_gameStarted && _firstTriggerTouched)
                StartGameplay();

            if (!_gameStarted || !_firstTriggerTouched) return;
            if (!_secondTriggerTouched) return;
            
            if (_currentLap < 3)
                ChangeLap();
            else if (_gameFinished == false)
                FinishGame();
        }

        private void SetLapTime()
        {
            var carExist = _playerCar != null && _carRecordManager != null;
            if (!carExist)
                return;
            
            var currentTime = _carRecordManager.GetCurrentTime();
            _gameUIController.SetCurrentTime(currentTime);
        }

        private void ChangeLap()
        {
            GenerateGhostCar();
            _currentLap++;
            _secondTriggerTouched = false;
            _gameUIController.LapFinished(_carRecordManager.GetRecordData().GetTotalTime());
            
            SetNewRecordManagerToCar();
            
            _firstTriggerTouched = false;
            _secondTriggerTouched = false;
        }

        private void SetNewRecordManagerToCar()
        {
            _carRecordManager.StopRecording();
            switch (_currentLap)
            {
                case 2:
                    _carRecordManager.SetRecordData(_carRecordDataLap2);
                    break;
                case 3:
                    _carRecordManager.SetRecordData(_carRecordDataLap3);
                    break;
                default:
                    _carRecordManager.SetRecordData(_carRecordDataLap1);
                    break;
            }
            _carRecordManager.StartRecording();
        }

        private void GenerateGhostCar()
        {
            CarRecordData bestRecordData = GetBetsTimeOnRecordData();
            if (bestRecordData.GetTotalTime() == 0)
                return;
            
            if (_gosthCar != null)
                Destroy(_gosthCar);
            
            _gosthRecordManager = Instantiate(_gosthReferenceRecordManager, transform.position, transform.rotation);
            _gosthRecordManager.SetRecordData(bestRecordData);
            _gosthCar = _gosthRecordManager.gameObject;
            _gosthRecordManager.StartPlaying();
        }

        private CarRecordData GetBetsTimeOnRecordData()
        {
            var bestTime = 0f;
            var bestRecordData = _carRecordDataLap3;
            foreach (var recordData in new []{_carRecordDataLap1, _carRecordDataLap2, _carRecordDataLap3})
            {
                if (recordData.GetTotalTime() >= bestTime && bestRecordData.GetTotalTime() != 0) continue;
                if (recordData.GetTotalTime() == 0) continue;
                
                bestTime = recordData.GetTotalTime();
                bestRecordData = recordData;
            }

            return bestRecordData;
        }
        
        private void FinishGame()
        {
            _gameFinished =true;
            _carRecordManager.StopRecording();
            _carRecordManager.SetRecordData(GetBetsTimeOnRecordData());
            _carRecordManager.StartPlaying();
            
            var carController = _playerCar.GetComponent<CarControl.CarController>();
            carController.enabled = false;
            
            _gosthRecordManager?.StopPlaying();
            _gameUIController.ShowFinalScreen(GetBetsTimeOnRecordData().GetTotalTime());
            OnGameFinished?.Invoke();
        }
    }
}