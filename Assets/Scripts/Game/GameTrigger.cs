using UnityEngine;
using UnityEngine.Events;

namespace Game
{
    public class GameTrigger : MonoBehaviour
    {
        public UnityEvent OnPlayerEnter;
        private void OnTriggerEnter(Collider other)
        {
            if (other.CompareTag("Player"))
            {
                OnPlayerEnter?.Invoke();
            }
        }
    }
}