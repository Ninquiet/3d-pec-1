using System;
using CarControl;
using UnityEngine;

namespace Game
{
    public class GameRoadTrigger : MonoBehaviour
    {
        private GameplayControl _gameplayControl;
        private CarController _carController;

        private void Awake()
        { 
            _gameplayControl = FindObjectOfType<GameplayControl>();
            if (_gameplayControl == null)
                throw new Exception("GameplayControl not found");
            
            _gameplayControl.OnGameReset += SetCarController;
        }

        private void SetCarController()
        {
            _carController = _gameplayControl.GetCurrentCar.transform.GetComponent<CarController>();
            if (_carController == null)
                throw new Exception("CarController not found");
        }

        private void OnTriggerEnter(Collider other)
        {
            Debug.Log("TriggerEntered");
            if (other.CompareTag("Player"))
            {
                _carController?.TouchingRoad(true);
            }
        }
        private void OnTriggerExit(Collider other)
        {
            Debug.Log("TriggerExited");
            if (other.CompareTag("Player"))
            {
                _carController?.TouchingRoad(false);
            }
        }
    }
}