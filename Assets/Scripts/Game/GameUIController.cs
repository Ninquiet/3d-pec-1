using System;
using DG.Tweening;
using TMPro;
using UnityEngine;

namespace Game
{
    public class GameUIController : MonoBehaviour
    {
        [SerializeField] private GameplayControl _gameplayControl;
        [Header("Lap Time")]
        [SerializeField] private CanvasGroup _lapTimeCanvasGroup;
        [SerializeField] private TMP_Text _lapTimeText;
        [Header("Final Screen")]
        [SerializeField] private CanvasGroup _finalScreenCanvasGroup;
        [SerializeField] private TMP_Text _finalScreenText;
        [Header("Current Time")]
        [SerializeField] private CanvasGroup _currentTimeCanvasGroup;
        [SerializeField] private TMP_Text _currentTimeText;
        [Header("Pause Screen")]
        [SerializeField] private CanvasGroup _pauseCanvasGroup;

        private bool _gamePaused;
        private bool _canPauseGame;
        public void SetCurrentTime(float currentTime)
        {
            _currentTimeText.text = FilterNumbers(currentTime).ToString();
        }

        public void PauseGame(bool isPauseGame)
        {
            if (!_canPauseGame) return;
            
            _gamePaused = isPauseGame;
            _pauseCanvasGroup.DOKill();
            if (!isPauseGame)
            {
                SetTimeScale(1);
                _pauseCanvasGroup.interactable = false;
                _pauseCanvasGroup.DOFade(0, 0.5f);
            }
            else
            {
                _pauseCanvasGroup.DOFade(1, 0.5f).OnComplete
                    (() =>
                    {
                        _pauseCanvasGroup.interactable = true;
                        SetTimeScale(0);
                    }); 
            }
        }
        
        public void SetTimeScale(float timeScale)
        {
            Time.timeScale = timeScale;
        }

        public void ShowFinalScreen(float bestTime)
        {
            _canPauseGame = false;
            _currentTimeCanvasGroup.DOFade(0, 0.5f);
            _finalScreenText.text = $"Best time: {FilterNumbers(bestTime)}";
            _finalScreenCanvasGroup.DOFade(1, 0.5f);
        }

        public void LapFinished(float lapTime)
        {
            _lapTimeText.text = $"Lap time: {FilterNumbers(lapTime)}";
            _lapTimeCanvasGroup.DOFade(1, 1f).OnComplete(() =>
            {
                _lapTimeCanvasGroup.DOFade(0, 2f);
            });
        }
        
        public void ExitGame()
        {
            Application.Quit();
        }
        
        private void Awake()
        {
            _gameplayControl.OnGameReset += Start;
            _gameplayControl.OnGameStarted += ShowCurrentTime;
        }

        private void Start()
        {
            _canPauseGame = true;
            _lapTimeCanvasGroup.alpha = 0;
            _finalScreenCanvasGroup.alpha = 0;
            _currentTimeCanvasGroup.alpha = 0;
            _pauseCanvasGroup.alpha = 0;
        }

        private void Update()
        {
            if (Input.GetKeyDown(KeyCode.Escape))
            {
                PauseGame(!_gamePaused);
            }
        }

        private void ShowCurrentTime()
        {
            _currentTimeText.text = "0.00";
            _currentTimeCanvasGroup.DOFade(1, 0.5f);
        }
        
        private float FilterNumbers(float number)
        {
            return (float) Math.Round(number, 2);
        }
    }
}