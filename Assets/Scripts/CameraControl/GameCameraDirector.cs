using System;
using Cinemachine;
using Game;
using UnityEngine;
using Random = UnityEngine.Random;

namespace CameraControl
{
    public class GameCameraDirector : MonoBehaviour
    {
        [SerializeField] private GameplayControl _gameplayControl;
        [SerializeField] private CinemachineVirtualCamera _mainCamera;
        [SerializeField] private CinemachineVirtualCamera[] _randomCameras;

        private bool _onRandomCameras;
        private float _changeBetweenCamerasTime = 5.0f;
        private float _currentTimeOnRandomCameraChange = 0.0f;

        public void UseMainCamera()
        {
            _onRandomCameras = false;
            DisableAllCameras();
            _mainCamera.Priority = 10;
            SetTarget(_mainCamera);
        }
        
        public void UseRandomCameras()
        {
            _onRandomCameras = true;
            ChangeToRandomCamera();
        }
        
        private void Awake()
        {
            _gameplayControl.OnGameReset += UseMainCamera;
            _gameplayControl.OnGameReset += UseMainCamera;
            _gameplayControl.OnGameFinished += UseRandomCameras;
        }

        private void Update()
        {
            if (_onRandomCameras)
            {
                var needToChangeRandomCamera = Time.time - _currentTimeOnRandomCameraChange >= _changeBetweenCamerasTime;
                if (needToChangeRandomCamera)
                {
                    ChangeToRandomCamera();
                }
            }
        }
        
        private void ChangeToRandomCamera()
        {
            _currentTimeOnRandomCameraChange = Time.time;
            DisableAllCameras();
            var randomCamera = _randomCameras[Random.Range(0, _randomCameras.Length)];
            randomCamera.Priority = 10;
            SetTarget(randomCamera);
        }

        private void DisableAllCameras()
        {
            foreach (var virtualCamera in _randomCameras)
            {
                virtualCamera.Priority = 0;
                virtualCamera.gameObject.SetActive(false);
            }
        }
        
        private void SetTarget(CinemachineVirtualCamera camera)
        {
            camera.Follow = _gameplayControl.GetCurrentCar.transform;
            camera.LookAt = _gameplayControl.GetCurrentCar.transform;
            camera.gameObject.SetActive(true);
        }
    }
}