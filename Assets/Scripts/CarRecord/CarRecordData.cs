using System;
using System.Collections.Generic;
using UnityEngine;

namespace CarRecord
{
    public struct CarLogSample
    {
        public Vector3 CarPosition;
        public Quaternion CarRotation;
        public float RecordTime;
    }

    [CreateAssetMenu(fileName = "CarRecordData", menuName = "CarGame/CarRecordData")]
    public class CarRecordData : ScriptableObject
    {
        public List<CarLogSample> _recordInfoList;

        private void OnEnable()
        {
            _recordInfoList = new List<CarLogSample>();
        }

        public void AddNewData(Transform carTransform, float snapTime)
        {
            _recordInfoList ??= new List<CarLogSample>();

            _recordInfoList.Add(new CarLogSample
            {
                CarPosition = carTransform.position,
                CarRotation = carTransform.rotation,
                RecordTime = snapTime
            });
        }

        public CarLogSample GetDataAt(int sample)
        {
            if (_recordInfoList == null || _recordInfoList.Count <= sample)
            {
                throw new ArgumentOutOfRangeException("sample", "The requested sample is out of range.");
            }
            return _recordInfoList[sample];
        }
        
        public float GetTotalTime()
        {
            if (_recordInfoList == null || _recordInfoList.Count == 0)
            {
                return 0.0f;
            }
            
            return _recordInfoList[_recordInfoList.Count - 1].RecordTime;
        }
        
        public int GetSamplesCount()
        {
            return _recordInfoList.Count;
        }

        public void Reset()
        {
            _recordInfoList.Clear();
        }
    }
}
