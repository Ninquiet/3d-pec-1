using UnityEngine;

namespace CarRecord
{
    public class RecordPlayer : RecordModuleBase
    {
        private float _lastLogTime;
        private CarLogSample _currentLogSample;
        private CarLogSample _nextLogSample;
        private int _currentSampleId = -1;
        private float _timeBetweenSamples;

        protected override void OnStarted()
        {
            _currentSampleId = 0;
            _lastLogTime = Time.time;
            if (_carRecordData.GetSamplesCount() > 0)
            {
                ChangeSample();
            }
        }

        protected override void OnStopped()
        {
        }

        public override void Update()
        {
            if (!_isEnabled) return;
            
            if (Time.time - _lastLogTime >= _timeBetweenSamples)
            {
                ChangeSample();
            }
            
            TranslateCar();
        }

        private void TranslateCar()
        {
            float elapsedSinceLastSample = Time.time - _lastLogTime;
            float percentageBetweenSamples = Mathf.Clamp01(elapsedSinceLastSample / _timeBetweenSamples);

            _carTransform.position = Vector3.Lerp(_currentLogSample.CarPosition, _nextLogSample.CarPosition, percentageBetweenSamples);
            _carTransform.rotation = Quaternion.Slerp(_currentLogSample.CarRotation, _nextLogSample.CarRotation, percentageBetweenSamples);
        }

        private void ChangeSample()
        {
            _currentSampleId++;
            if (_currentSampleId < _carRecordData.GetSamplesCount() - 1)
            {
                _currentLogSample = _carRecordData.GetDataAt(_currentSampleId);
                _nextLogSample = _carRecordData.GetDataAt(_currentSampleId + 1);
                _timeBetweenSamples = _nextLogSample.RecordTime - _currentLogSample.RecordTime;
                _lastLogTime = Time.time;
            }
            else
            {
                StopAction();
            }
        }
    }
}