using System;
using UnityEngine;

namespace CarRecord
{
    public class CarRecordManager : MonoBehaviour
    {
        [SerializeField] private CarRecordData _carRecordData;
        
        private RecordLogger _recordLogger = new ();
        private RecordPlayer _recordPlayer = new ();
        
        private RecordModuleBase _currentAction;
        
        public float GetCurrentTime()
        {
            return _currentAction.CurrentActionTime;
        }
        
        public void SetRecordData(CarRecordData carRecordData)
        {
            _carRecordData = carRecordData;
        }

        public void StartRecording()
        {
            StopOtherActions();
            SetCurrentAction(_recordLogger);
            _recordLogger.StartAction(transform, _carRecordData);
        }
        
        public CarRecordData GetRecordData()
        {
            return _carRecordData;
        }
        
        public void StopRecording()
        {
            _recordLogger.StopAction();
            SetCurrentAction(null);
        }
        
        public void StartPlaying()
        {
            StopOtherActions();
            SetCurrentAction(_recordPlayer);
            _recordPlayer.StartAction(transform, _carRecordData);
        }
        
        public void StopPlaying()
        {
            _recordPlayer.StopAction();
            SetCurrentAction(null);
        }

        private void StopOtherActions()
        {
            _recordPlayer.StopAction();
            _recordLogger.StopAction();
        }
        
        private void Update()
        {
            _currentAction?.Update();
        }

        private void SetCurrentAction(RecordModuleBase recordModuleBase)
        {
            _currentAction = recordModuleBase;
        }
    }
}