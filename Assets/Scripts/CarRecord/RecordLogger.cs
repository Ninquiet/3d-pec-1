using System;
using UnityEngine;

namespace CarRecord
{
    public class RecordLogger : RecordModuleBase
    {
        private static float TimeBetweenSamples = 0.25f;
        private const float BASE_TIME_BETWEEN_SAMPLES = 0.25f;
        private float _lastLogTime = 0.0f;
        
        public static void ChangeTimeBetweenSamples(float newRatioValue)
        {
            TimeBetweenSamples = newRatioValue * BASE_TIME_BETWEEN_SAMPLES;
        }
        
        protected override void OnStarted()
        {
            _carRecordData.Reset();
            _startingActionTime = Time.time;
            _lastLogTime = _startingActionTime;
            CreateRecordData();
        }
        
        protected override void OnStopped()
        {
            CreateRecordData();
        }

        public override void Update()
        {
            if (!_isEnabled)
                return;
            CheckTimeBetweenSamples();
        }

        private void CheckTimeBetweenSamples()
        {
            if (_lastLogTime + TimeBetweenSamples <= Time.time)
            {
                CreateRecordData();
                _lastLogTime = Time.time;
            }
        }

        private void CreateRecordData()
        {
            _carRecordData.AddNewData(_carTransform, CurrentActionTime);
        }
    }
}