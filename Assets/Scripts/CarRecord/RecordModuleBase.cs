using UnityEngine;

namespace CarRecord
{
    public abstract class RecordModuleBase
    {
        public float CurrentActionTime => Time.time - _startingActionTime;
        
        protected Transform _carTransform;
        protected CarRecordData _carRecordData;
        protected bool _isEnabled = false;
        protected float _startingActionTime = 0.0f;
        
        public abstract void Update();
        protected abstract void OnStopped();
        protected abstract void OnStarted();

        public virtual void StartAction(Transform carTransform, CarRecordData carRecordData)
        {
            if (carRecordData == null || carTransform == null)
            {
                Debug.LogError($"There are null references on {this}");
                return;
            }
            
            _carTransform = carTransform;
            _carRecordData = carRecordData;
            _isEnabled = true;
            _startingActionTime = Time.time;
            
            OnStarted();
        }

        public virtual void StopAction()
        {
            if (!_isEnabled)
                return;
            
            _isEnabled = false;
            OnStopped();
        }
    }
}