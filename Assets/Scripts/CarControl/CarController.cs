using UnityEngine;

namespace CarControl {
    public class CarController : MonoBehaviour
    {
        [SerializeField] Rigidbody _rigidBody;
        
        [SerializeField] WheelCollider _frontLeftWheel;
        [SerializeField] WheelCollider _frontRightWheel;
        [SerializeField] WheelCollider _rearLeftWheel;
        [SerializeField] WheelCollider _rearRightWheel;
        
        [Space(10)]
        [SerializeField] private Transform _frontLeftWheelMesh;
        [SerializeField] private Transform _frontRightWheelMesh;
        [SerializeField] private Transform _rearLeftWheelMesh;
        [SerializeField] private Transform _rearRightWheelMesh;

        public float acceleration = 500f;
        public float breakingForce = 300f;
        public float maxTurnAngle = 15f;
        
        private float _currentAcceleration = 0;
        private float _currentBreakForce = 0;
        private float _currentTurnAngle = 0;
        private bool _isTouchingRoad = false;
        private float GetAcceleration => acceleration * (_isTouchingRoad ? 1 : 0.3f);
        
        public void TouchingRoad(bool isTouching)
        {
            _isTouchingRoad = isTouching;
        }

        private void FixedUpdate()
        {
            CheckIfCarIsUpsideDown();
            CheckInputs();
            UpdateAcceleration();
            UpdateTorqueValues();
            UpdateTurn();
            UpdateWheel();
        }

        private void CheckIfCarIsUpsideDown()
        {
            if (transform.up.y < 0)
            {
                transform.position += Vector3.up * 5;
                transform.rotation = Quaternion.Euler(0, transform.rotation.eulerAngles.y, 0);
                // anulate any force on rigidbody
                _rigidBody.velocity = Vector3.zero;
                _rigidBody.angularVelocity = Vector3.zero;
                _rigidBody.isKinematic = true;
            }
            else if (_rigidBody.isKinematic)
            {
                _rigidBody.isKinematic = false;
            }
        }

        private void CheckInputs()
        {
            _currentAcceleration = GetAcceleration * Input.GetAxis("Vertical");

            if (Input.GetKey(KeyCode.Space))
                _currentBreakForce = breakingForce;
            else
                _currentBreakForce = 0;
        }
        
        private void UpdateAcceleration()
        {
            _frontLeftWheel.motorTorque = _currentAcceleration;
            _frontRightWheel.motorTorque = _currentAcceleration;
        }

        private void UpdateTorqueValues()
        {
            _frontRightWheel.brakeTorque = _currentBreakForce;
            _frontLeftWheel.brakeTorque = _currentBreakForce;
            _rearRightWheel.brakeTorque = _currentBreakForce;
            _rearLeftWheel.brakeTorque = _currentBreakForce;
        }
        
        private void UpdateTurn()
        {
            _currentTurnAngle = maxTurnAngle * Input.GetAxis("Horizontal");
            _frontRightWheel.steerAngle = _currentTurnAngle;
            _frontLeftWheel.steerAngle = _currentTurnAngle;
        }
        
        private void UpdateWheel()
        {
            UpdateSingleWheel(_frontLeftWheel, _frontLeftWheelMesh);
            UpdateSingleWheel(_frontRightWheel, _frontRightWheelMesh);
            UpdateSingleWheel(_rearLeftWheel, _rearLeftWheelMesh);
            UpdateSingleWheel(_rearRightWheel, _rearRightWheelMesh);
        }

        private void UpdateSingleWheel(WheelCollider wheelCollider, Transform visualWheel)
        {
            Vector3 pos;
            Quaternion rot;
            wheelCollider.GetWorldPose(out pos, out rot);
            visualWheel.position = pos;
            visualWheel.rotation = rot;
        }
    }
}