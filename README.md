# Programación 3D  Entrega de la actividad R1 - CARRERAS CONTRARELOJ
![Carreras Contra reloj](https://i.ibb.co/5MQcHdN/image-2024-04-08-150813755.png)

# Video:
https://youtu.be/yhKHteLQ37M

# Build:
Puedes jugar el juego acá:
https://simmer.io/@Ninquiet/carrera-contra-reloj-v1-pec1

# Resumen:
Es un juego de carreras en el qué puedes correr contra ti mismo, el funcionamiento es simple, solo necesitas las WASD para moverte, y Space Bar para frenar

**Todos los Assets los he creado yo, las texturas las hice con photoshop y el personaje lo cree con primitvas**

Cree tambien mi propio sistema para grabar y reproducir las grabaciones (es una version personalizada :3 )

# Funcionamiento:

![Diagrama](https://i.ibb.co/424CyX6/image-2024-04-08-145753641.png)

### Game:

El principal es ```GamePlayeControl``` (que se encarga de organizar el inicio transicion y final de la partida), luego está ```GameUIController``` (que controla todo lo relacionado a UI), y por ultimo ```GameTrigger```, que es el componente usado para tener un tracking de cuando el usuario pasa pro ciertas parrtes o no).

### CarControl: 

Compuesto de ```CarController.cs```, es el que se encarga del movimiento del carro (lo he personalizado un poco)

### CarRecord:

Compuesto de ```CarRecordDat```a (Serializable donde se guarda la informacion) ```CarRecordManager``` (que es el que gestiona cuando grabar y cuando reproducir) ```RecordLogger``` (encargado de grabar lo snapshots) ```RecordPlayer``` (encargado de reproducir los snapshots), y ```RecordModuleBase```, que es la clase abstracta base para el logger y el RecordPlayer

### CameraDirector:

Compuesto unicamente por ```GameCameraDirector.cs``` es el encargado de controlar que tipo de camara se usará, así, cuando el juego termina, este al estar conectado con un evento en ```GameplayControl``` es notificado, y empieza a cambiar entre diferentes camaras. para esto he usado CineMachine para tener un mejor acabado

# Assets usados:

[Free Trees](https://assetstore.unity.com/packages/3d/vegetation/trees/free-trees-103208)
Para agregar arboles (debido a que estoy usando URP)

[DoTween](https://assetstore.unity.com/packages/tools/animation/dotween-hotween-v2-27676)
Para hacer transiciones rapidamente.


